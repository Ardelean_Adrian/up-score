class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def hello
    render html: '<h1>Hello, world</h1>'.html_safe
  end

  private

  def logged_in?
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def authorize
    redirect_to login_path, notice: 'You need to login' unless logged_in?
  end

  helper_method :logged_in?
end
